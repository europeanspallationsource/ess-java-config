pipeline {
    agent {
        docker {
            image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
            label 'docker'
        }
    }

    environment {
        GIT_TAG = sh(returnStdout: true, script: 'git describe --exact-match || true').trim()
    }

    stages {
        stage('Install') {
            steps {
                sh 'mvn clean install'
            }
        }
        stage('Deploy') {
            when {
                not { environment name: 'GIT_TAG', value: '' }
            }
            steps {
                // WARNING! The file to be uploaded shall be on the jenkins slave and not inside the docker container!
                // The workspace is mounted automatically by jenkins as a volume, so we can use the current directory
                sh "cp ${HOME}/.m2/repository/se/esss/ics/ess-java-config/${GIT_TAG}/ess-java-config-${GIT_TAG}.pom ."
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "ess-java-config-${GIT_TAG}.pom",
                          "target": "libs-release-local/se/esss/ics/ess-java-config/${GIT_TAG}/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
